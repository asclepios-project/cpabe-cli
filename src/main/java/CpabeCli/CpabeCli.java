/*
   Copyright 2021 ASCLEPIOS project

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package CpabeCli;

import co.junwei.bswabe.*;
import co.junwei.cpabe.Common;
import co.junwei.cpabe.policy.LangPolicy;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class CpabeCli {
    private static final String utilityName = "java -jar CpabeCli.jar";
    private static boolean dryRun;

    public static void main(String[] args) {
        try  {
            exceptionBasedMain(args);
        }
        catch(ExitException e)  {
            System.err.println(e.getMessage());
            System.out.println("check "+utilityName+" --help for help");
            System.exit(e.getExitCode());
        }
        System.exit(0);
    }

    /**
     * contains main functionality but does not call System.exit but thorws ExitException
     * This function may be called directly from unit tests because it does not exit the process.
     * @param args the command line argument list
     * @return exit code
     */
    public static void exceptionBasedMain(String[] args)  throws ExitException  {
        // ===== parse CLI arguments =====
        Options options = new Options();

        Option dryRun = new Option("d","dry-run",false,"do not touch write anything to disk");
            options.addOption(dryRun);
        Option masterKeyFilename = new Option("m", "master-key", true, "master key file");
            masterKeyFilename.setArgName("master key file");
            options.addOption(masterKeyFilename);
        Option publicKeyFilename = new Option("p", "public-key", true, "public key file");
            publicKeyFilename.setArgName("public key file");
            options.addOption(publicKeyFilename);
        Option userKeyFilename = new Option("u", "user-key", true, "user key file");
            userKeyFilename.setArgName("user key file");
            options.addOption(userKeyFilename);
        Option userKeyAttributes = new Option("a","attributes",true,"user key attributes string");
            userKeyAttributes.setArgName("attributes string");
            options.addOption(userKeyAttributes);
        Option commandGenerateMasterKeypair = new Option("g","generate-master-keypair",false,"generate a master keypair");
            options.addOption(commandGenerateMasterKeypair);
        Option commandGenerateUserKey = new Option("k","generate-user-key",false,"generate a user key (implies -u)");
            options.addOption(commandGenerateUserKey);
        Option help = new Option("h","help",false,"show this help message");
            options.addOption(help);

        CommandLineParser commandLineParser = new DefaultParser();
        HelpFormatter helpFormatter = new HelpFormatter();

        CommandLine commandLine = null;

        try  {
            commandLine = commandLineParser.parse(options,args);
        }
        catch (ParseException e)  {
            throw new ExitException(1, e.getMessage());
        }

        CpabeCli.dryRun = commandLine.hasOption(dryRun.getOpt());

        if(commandLine.hasOption(help.getOpt()))  {
            helpFormatter.printHelp(utilityName,options,true);
            return ;
        }
        else if(commandLine.hasOption(commandGenerateMasterKeypair.getOpt()))  {
            if( ! commandLine.hasOption(masterKeyFilename.getOpt()))
                throw new ExitException(1, "no master key file specified");
            if( ! commandLine.hasOption(publicKeyFilename.getOpt()))
                throw new ExitException(1, "no public key file specified");
            generateMasterKeypair(commandLine.getOptionValue(masterKeyFilename.getOpt()), commandLine.getOptionValue(publicKeyFilename.getOpt())  );
        }
        else if(commandLine.hasOption(commandGenerateUserKey.getOpt()))  {
            if( ! commandLine.hasOption(userKeyFilename.getOpt()) )
                throw new ExitException(5, "no user key filename specified");
            if( ! commandLine.hasOption(userKeyAttributes.getOpt()) )
                throw new ExitException(7, "no user key attributes string specified");
            generateUserKey( commandLine.getOptionValue(masterKeyFilename.getOpt()),
                    commandLine.getOptionValue(publicKeyFilename.getOpt()),
                    commandLine.getOptionValue(userKeyFilename.getOpt()),
                    commandLine.getOptionValue(userKeyAttributes.getOpt()) );
        }
        else  {
            throw new ExitException(2, "no command selected");
        }
    }

    /**
     * generate a master keypair
     * @param masterKeyFilename file name to save the master key to. can either already exist or not but must be writable.
     * @param publicKeyFilename file name to save the public key to. can either already exist or not but must be writable.
     */
    public static void generateMasterKeypair(String masterKeyFilename, String publicKeyFilename)  throws ExitException  {
        File masterKeyFile = new File(masterKeyFilename);
        File publicKeyFile = new File(publicKeyFilename);

        // check write access rights
        // note: this is done explicitly, because no exceptions would be raised in dry run mode
        for(File file : new File[]{masterKeyFile,publicKeyFile})  {
            // check if file is writable or if it does not exist if the parent directory is writable
            if(  ( file.exists() && ! file.canWrite() )
            ||   ( ! file.exists() && ! (file.getAbsoluteFile().getParentFile().isDirectory() && file.getAbsoluteFile().getParentFile().canWrite() ) )  )  {
                throw new ExitException(3, "cannot write to " + file.getAbsolutePath());
            }
        }

        // generate keypair
        BswabePub bswabePublicKey = new BswabePub();
        BswabeMsk bswabeMasterKey = new BswabeMsk();
        Bswabe.setup(bswabePublicKey,bswabeMasterKey);

        // store to files
        if( ! dryRun )  {
            // public key file
            byte[] publicKeyBytes = SerializeUtils.serializeBswabePub(bswabePublicKey);
            try  {
                Common.spitFile(publicKeyFile.getAbsolutePath(), publicKeyBytes);
            }
            catch(IOException e)  {
                throw new ExitException(3, "cannot write to " + publicKeyFile.getAbsolutePath());
            }
            // master key file
            byte[] masterKeyBytes = SerializeUtils.serializeBswabeMsk(bswabeMasterKey);
            try  {
                Common.spitFile(masterKeyFile.getAbsolutePath(), masterKeyBytes);
            }
            catch (IOException e)  {
                throw new ExitException(3,"cannot write to " + masterKeyFile.getAbsolutePath());
            }
        }
    }

    /**
     * generate a user key from an existing master keypair
     * @param masterKeyFilename file name of an existing master key
     * @param publicKeyFilename file name of an existing public key
     * @param userKeyFilename file name to save to user key to. can either already exist or not but must be writable.
     */
    public static void generateUserKey(String masterKeyFilename, String publicKeyFilename, String userKeyFilename, String attributes)  throws ExitException  {
        File masterKeyFile = new File(masterKeyFilename);
        File publicKeyFile = new File(publicKeyFilename);
        File userKeyFile = new File(userKeyFilename);

        // check file access rights
        // note: this is done explicitly, because no exceptions would be raised in dry run mode
        for(File file : new File[]{masterKeyFile,publicKeyFile})  {
            if( ! file.canRead() )  {
                throw new ExitException(4,"cannot read " + file.getAbsolutePath());
            }
        }
        if(  ( userKeyFile.exists() && ! userKeyFile.canWrite() )
        ||   ( !userKeyFile.exists() && ! (userKeyFile.getAbsoluteFile().getParentFile().isDirectory() && userKeyFile.getAbsoluteFile().getParentFile().canWrite() ) )  ) {
            throw new ExitException(6,"cannot write to " + userKeyFile.getAbsolutePath());
        }

        // read master and public key from files
        BswabePub bswabePublicKey;
        BswabeMsk bswabeMasterKey;
        try {
            bswabePublicKey = SerializeUtils.unserializeBswabePub(Common.suckFile(publicKeyFile.getAbsolutePath()));
        }
        catch(IOException e)  {
            throw new ExitException(4, "cannot read " + publicKeyFile.getAbsolutePath());
        }
        try {
            bswabeMasterKey = SerializeUtils.unserializeBswabeMsk(bswabePublicKey, Common.suckFile(masterKeyFile.getAbsolutePath()));
        }
        catch(IOException e)  {
            throw new ExitException(4, "cannot read " + masterKeyFile.getAbsolutePath());
        }

        // generate user private key
        BswabePrv bswabeUserKey;
        try {
            bswabeUserKey = Bswabe.keygen(bswabePublicKey, bswabeMasterKey, LangPolicy.parseAttribute(attributes));
        }
        catch (NoSuchAlgorithmException e)  {
            throw new ExitException(8, e.getMessage());
        }

        // store to file
        if( ! dryRun )  {
            byte[] userKeyBytes = SerializeUtils.serializeBswabePrv(bswabeUserKey);
            try  {
                Common.spitFile(userKeyFile.getAbsolutePath(),userKeyBytes);
            }
            catch (IOException e)  {
                throw new ExitException(6, "cannot write to " + userKeyFile.getAbsolutePath());
            }
        }
    }
}