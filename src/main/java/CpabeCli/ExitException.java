package CpabeCli;

public class ExitException  extends Throwable  {
    private final int exitCode;
    private final String message;
    public ExitException(int exitCode, String message)  {
        this.exitCode = exitCode;
        this.message = message;
    }
    public int getExitCode()  {
        return exitCode;
    }
    public String getMessage()  {
        return message;
    }
}
