/*
   Copyright 2021 ASCLEPIOS project

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package CpabeCli;

import co.junwei.cpabe.Common;
import org.junit.*;
import org.junit.contrib.java.lang.system.ExpectedSystemExit ;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

public class Main {
    @Rule
    public final ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File masterKeyFile;
    private File publicKeyFile;
    private File userKeyFile;
    private String fileParameters;

    @Before
    public void Before()  throws IOException,SecurityException  {
        masterKeyFile = new File(temporaryFolder.getRoot(),"master_key");
            masterKeyFile.createNewFile();
            masterKeyFile.setReadable(true);
            masterKeyFile.setWritable(true);
        publicKeyFile = new File(temporaryFolder.getRoot(),"public_key");
            publicKeyFile.createNewFile();
            publicKeyFile.setReadable(true);
            publicKeyFile.setWritable(true);
        userKeyFile = new File(temporaryFolder.getRoot(), "user_key");
            userKeyFile.createNewFile();
            userKeyFile.setReadable(true);
            userKeyFile.setWritable(true);
        fileParameters = " -m "+masterKeyFile.getAbsolutePath()+" -p "+publicKeyFile.getAbsolutePath()+" -u"+userKeyFile.getAbsolutePath()+" ";
    }



    /**
     * checks whether the application parses its command line parameters correctly and finds inconsistent parameter
     * combinations
     */

    @Test
    public void requireMasterKeyFile()  {
        expectedSystemExit.expectSystemExitWithStatus(1);
        CpabeCli.main(("--generate-master-keypair -p dummy"+publicKeyFile.getAbsolutePath()).split(" "));
    }

    @Test
    public void requirePublicKeyFile()  {
        expectedSystemExit.expectSystemExitWithStatus(1);
        CpabeCli.main(("--generate-master-keypair -m "+masterKeyFile.getAbsolutePath()).split(" "));
    }

    @Test
    public void requireCommand()  {
        expectedSystemExit.expectSystemExitWithStatus(2);
        CpabeCli.main(("-m "+masterKeyFile.getAbsolutePath()+" -p "+publicKeyFile.getAbsolutePath()).split(" "));
    }

    @Test
    public void checkGoodFileAccessRights()  {
        expectedSystemExit.expectSystemExitWithStatus(0);
        masterKeyFile.setReadable(true);
        masterKeyFile.setWritable(true);
        publicKeyFile.setReadable(true);
        publicKeyFile.setWritable(true);
        CpabeCli.main(("-g "+fileParameters).split(" "));
    }

    @Test
    public void checkBadFileAccessRights()  {
        Assume.assumeFalse(ContainerDetector.isRunningInsideDocker());
        expectedSystemExit.expectSystemExitWithStatus(3);
        masterKeyFile.setReadable(true);
        masterKeyFile.setWritable(false);
        publicKeyFile.setReadable(true);
        publicKeyFile.setWritable(false);
        CpabeCli.main(("-g "+fileParameters).split(" "));
    }

    /**
     * checks whether the application generates keys correctly
     */
    @Test
    public void obeyDryRunOnGenerateMasterKeypairCommand()  throws IOException,ExitException  {
        CpabeCli.exceptionBasedMain(new String[]{ "--generate-master-keypair", "--dry-run",
                "-m", masterKeyFile.getAbsolutePath(), "-p", publicKeyFile.getAbsolutePath()});
        Assert.assertEquals(Common.suckFile(masterKeyFile.getAbsolutePath()).length, 0);
        Assert.assertEquals(Common.suckFile(publicKeyFile.getAbsolutePath()).length, 0);
    }

    @Test
    public void plausibleKey()  throws IOException,ExitException  {
        CpabeCli.exceptionBasedMain(new String[]{ "--generate-master-keypair", "-m", masterKeyFile.getAbsolutePath(),
                "-p", publicKeyFile.getAbsolutePath()});

        String masterKeyBase64 = new String(Base64.getEncoder().encode(Common.suckFile(masterKeyFile.getAbsolutePath())));
        String publicKeyBase64 = new String(Base64.getEncoder().encode(Common.suckFile(publicKeyFile.getAbsolutePath())));

        if(masterKeyBase64.length() != 208)
            throw new AssertionError("master key has wrong length");
        if(publicKeyBase64.length() != 1188)
            throw new AssertionError("public key has wrong length");
        if( ! masterKeyBase64.startsWith("AAAAF"))
            throw new AssertionError("master key starts with unplausible bytes");
        if( ! publicKeyBase64.startsWith("AAABZ3R5cGUgYQpxIDg3ODA3MTA3OTk2NjMzMTI1MjI0Mzc3ODE5ODQ3NTQwNDk4MTU4MDY4ODMxOTk0MTQyMDgyMTEwMjg2NTMzOTkyNjY0NzU2MzA4ODAyMjI5NTcwNzg2MjUxNzk0MjI2NjIyMjE0MjMxNTU4NTg3Njk1ODIzMTc0NTkyNzc3MTMzNjczMTc0ODEzMjQ5MjUxMjk5OTgyMjQ3OTEKaCAxMjAxNjAxMjI2NDg5MTE0NjA3OTM4ODgyMTM2Njc0MDUzNDIwNDgwMjk1NDQwMTI1MTMxMTgyMjkxOTYxNTEzMTA0NzIwNzI4OTM1OTcwNDUzMTEwMjg0NDgwMjE4MzkwNjUzNzc4Njc3NgpyIDczMDc1MDgxODY2NTQ1MTYyMTM2MTExOTI0NTU3MTUwNDkwMTQwNTk3NjU1OTYxNwpleHAyIDE1OQpleHAxIDEwNwpzaWduMSAxCnNpZ24wIDEK"))
            throw new AssertionError("public key starts with unplausible bytes");
    }
}
