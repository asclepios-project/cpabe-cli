package CpabeCli;

import co.junwei.cpabe.Common;
import org.junit.*;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.rules.TemporaryFolder;

import java.beans.Encoder;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

public class GenenerateUserKey {
    @Rule
    public final ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File masterKeyFile;
    private File publicKeyFile;
    private File userKeyFile;

    @Before
    public void Before()  throws IOException,SecurityException,ExitException  {
        // generate master keypair
        masterKeyFile = new File(temporaryFolder.getRoot(),"master_key");
        masterKeyFile.createNewFile();
        masterKeyFile.setReadable(true);
        masterKeyFile.setWritable(true);
        publicKeyFile = new File(temporaryFolder.getRoot(),"public_key");
        publicKeyFile.createNewFile();
        publicKeyFile.setReadable(true);
        publicKeyFile.setWritable(true);
        CpabeCli.exceptionBasedMain(new String[]{"--generate-master-keypair", "-m", masterKeyFile.getAbsolutePath(),
                "-p", publicKeyFile.getAbsolutePath()});

        // create user key file but do not write anything to it
        userKeyFile = new File(temporaryFolder.getRoot(), "user_key");
            userKeyFile.createNewFile();
            userKeyFile.setReadable(true);
            userKeyFile.setWritable(true);
    }

    @Test
    public void requireUserKeyFile()  {
        expectedSystemExit.expectSystemExitWithStatus(5);
        CpabeCli.main(new String[]{"--generate-user-key", "-m",masterKeyFile.getAbsolutePath(),
                "-p",publicKeyFile.getAbsolutePath()});
    }

    @Test
    public void checkGoodFileAccessRights()  throws ExitException  {
        masterKeyFile.setReadable(true);
        masterKeyFile.setWritable(false);
        publicKeyFile.setReadable(true);
        masterKeyFile.setWritable(false);
        userKeyFile.setReadable(false);
        userKeyFile.setWritable(true);
        CpabeCli.exceptionBasedMain(new String[]{"--generate-user-key", "--attributes", "role:dummy",
                "-m", masterKeyFile.getAbsolutePath(), "-p", publicKeyFile.getAbsolutePath(),
                "-u", userKeyFile.getAbsolutePath() });
    }

    @Test
    public void checkBadFileAccessRightsToMasterKeyAndPublicKey()  {
        Assume.assumeFalse(ContainerDetector.isRunningInsideDocker());
        expectedSystemExit.expectSystemExitWithStatus(4);
        masterKeyFile.setReadable(false);
        masterKeyFile.setWritable(true);
        publicKeyFile.setReadable(false);
        publicKeyFile.setWritable(true);
        userKeyFile.setReadable(false);
        userKeyFile.setWritable(true);
        CpabeCli.main(new String[]{"--generate-user-key", "--attributes", "role:dummy",
                "-m", masterKeyFile.getAbsolutePath(), "-p", publicKeyFile.getAbsolutePath(),
                "-u", userKeyFile.getAbsolutePath() });
    }
    @Test
    public void checkBadFileAccessRightsToUserKey()  {
        Assume.assumeFalse(ContainerDetector.isRunningInsideDocker());
        expectedSystemExit.expectSystemExitWithStatus(6);
        masterKeyFile.setReadable(true);
        masterKeyFile.setWritable(false);
        publicKeyFile.setReadable(true);
        publicKeyFile.setWritable(false);
        userKeyFile.setReadable(true);
        userKeyFile.setWritable(false);
        CpabeCli.main(new String[]{"--generate-user-key", "--attributes", "role:dummy",
                "-m", masterKeyFile.getAbsolutePath(), "-p", publicKeyFile.getAbsolutePath(),
                "-u", userKeyFile.getAbsolutePath() });
    }

    @Test
    public void obeyDryRunOnGenerateUserKeyCommand()  throws IOException,ExitException  {
        CpabeCli.exceptionBasedMain(new String[]{"--generate-user-key", "--attributes", "role:dummy", "--dry-run",
                "-m", masterKeyFile.getAbsolutePath(), "-p", publicKeyFile.getAbsolutePath(),
                "-u", userKeyFile.getAbsolutePath()});
        Assert.assertEquals(Common.suckFile(userKeyFile.getAbsolutePath()).length, 0);
    }

    @Test
    public void plausibleKey()  throws ExitException,IOException  {
        CpabeCli.exceptionBasedMain(new String[]{"--generate-user-key", "--attributes", "role:dummy",
                "-m", masterKeyFile.getAbsolutePath(), "-p", publicKeyFile.getAbsolutePath(),
                "-u", userKeyFile.getAbsolutePath()});
        String userKeyBase64 = new String(Base64.getEncoder().encode(Common.suckFile(userKeyFile.getAbsolutePath())));

        // @TODO check key length (depends on attributes)
        //if(userKeyBase64.length() != 552)
        //    throw new AssertionError("user key has wrong length");
        if( ! userKeyBase64.startsWith("AAAAg"))
            throw new AssertionError("user key starts with unplausible bytes");
    }
}
